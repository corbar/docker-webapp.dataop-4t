

```bash
# create the database server
doc run -d -it --net corbanet --name mysql --hostname mysql -e MYSQL="mysql" --memory=1G --cpu-period=100000 --cpu-quota=50000 corba/mysql.dataop



# create the analytics cluster
doc run -d -it --net corbanet --name web-worker-01.analytics.dataop --hostname web-worker-01.analytics.dataop -e MYSQL="mysql" --memory=256M --cpu-period=100000 --cpu-quota=12500 corba/webapp.dataop-4t
doc run -d -it --net corbanet --name web-worker-02.analytics.dataop --hostname web-worker-02.analytics.dataop -e MYSQL="mysql" --memory=256M --cpu-period=100000 --cpu-quota=12500 corba/webapp.dataop-4t

# create the analytics load balancer
doc run -d -t --name load-balancer.analytics.dataop --net corbanet --hostname load-balancer.analytics.dataop --memory=1G --cpu-period=100000 --cpu-quota=50000 corba/load-balancer

# add the analytics workers to the load-balancer
doc exec load-balancer.analytics.dataop /lb-add-worker.sh web-worker-01.analytics.dataop:9201
doc exec load-balancer.analytics.dataop /lb-add-worker.sh web-worker-02.analytics.dataop:9201

# to remove use
# doc exec load-balancer.analytics.dataop /lb-rm-worker.sh web-worker-01



# create the service cluster
doc run -d -it --net corbanet --name web-worker-01.service.dataop --hostname web-worker-01.service.dataop --memory=256M --cpu-period=100000 --cpu-quota=12500 corba/webapp.dataop-4t
doc run -d -it --net corbanet --name web-worker-02.service.dataop --hostname web-worker-02.service.dataop --memory=256M --cpu-period=100000 --cpu-quota=12500 corba/webapp.dataop-4t

# create the service load-balancer
doc run -d -t -p 80:8080 --name load-balancer.service.dataop --net corbanet --hostname load-balancer.service.dataop --memory=1G --cpu-period=100000 --cpu-quota=50000 corba/load-balancer

# add the service workers to the load-balancer
doc exec load-balancer.service.dataop /lb-add-worker.sh web-worker-01.service.dataop:9201
doc exec load-balancer.service.dataop /lb-add-worker.sh web-worker-02.service.dataop:9201

# to remove use
# doc exec load-balancer.service.dataop /lb-rm-worker.sh web-worker-01
```
